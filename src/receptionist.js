// TODO: Write a class `Receptionist`. Please review the code
// in the unit test to understand its usage.
// <-start-
class Receptionist {
  constructor (idList = [], List = []) {
    this.idList = idList;
    this.List = List;
  }

  register (str) {
    if (str === '' || str === {} || str === null || str === undefined || {}.hasOwnProperty.call(str, 'id') === false || {}.hasOwnProperty.call(str, 'name') === false) {
      throw new Error('Invalid person');
    } else if (this.idList.includes(str.id)) {
      throw new Error('Person already exist');
    } else {
      this.idList.push(str.id);
      this.List.push(str);
      return this.List;
    };
  }

  getPerson (idx) {
    if (this.idList.includes(idx)) {
      for (let i = 0; i <= this.List.length; i++) {
        while (this.List[i].id === idx) {
          return this.List[i];
        }
      }
    } else {
      throw new Error('Person not found');
    }
  }

}
// --end-->

export default Receptionist;
