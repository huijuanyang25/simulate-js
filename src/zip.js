// TODO: Write a function `zip`. Please review the code in
// the unit test to understand its usage.
// <-start-
function zip (left, right) {
  if (left.length === 0 || right.length === 0) {
    return [];
  }

  if (left.length <= right.length) {
    const zippedArray = left.map((item, idx) => [item, right[idx]]);
    return zippedArray;
  } else {
    const zippedArray = right.map((item, idx) => [left[idx], item]);
    return zippedArray;
  }
};
// --end->

export default zip;
